# Initial release

 - D-Bus
   - Talk to
   - Own
 - Modules
   - Drag & drop to reorder
   - Delete
 - Filesystem
   - Custom locations
 - Extensions
 - Save JSON to new file
 - Open JSON files
 
# Future

 - YAML
 - Search
 - Edit extensions
 - Build the manifest using flatpak-builder
