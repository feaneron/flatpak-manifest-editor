/* window.js
 *
 * Copyright 2021 Georges Basile Stavracas Neto
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

const { Adw, GObject, Gtk } = imports.gi;

const Rows = imports.rows;
const Section = imports.section;

const validateEmptyEntry = row => {
    if (!row.visible)
        return undefined;

    const trimmed = row.text.trim();
    return trimmed ? trimmed : undefined;
};

var FmwModuleRow = GObject.registerClass({
    GTypeName: 'FmwModuleRow',
    Template: 'resource:///org/flatpak/ManifestEditor/module-row.ui',
    Signals: { 'update': {} },
    InternalChildren: ['icon', 'urlLabel'],
}, class FmwModuleRow extends Adw.ExpanderRow {
    _init(source) {
        super._init();

        this._data =  {
            'name': this._addRow(Rows.EntryRow, {
                title: _('Name'),
                validateFunc: validateEmptyEntry,
            }),
            'sources': [
                {
                    'type': this._addRow(Rows.ComboRow, {
                        title: _('Type'),
                        options: [
                            {
                                title: _('Git'),
                                value: 'git',
                            },
                            {
                                title: _('Archive'),
                                value: 'archive',
                            },
                            {
                                title: _('Extra data'),
                                value: 'extra-data',
                            },
                        ],
                    }),
                    'url': this._addRow(Rows.EntryRow, { title: _('URL') }),
                    'commit': this._addRow(Rows.EntryRow, { title: _('Commit') }),
                    'tag': this._addRow(Rows.EntryRow, { title: _('Tag') }),
                    'sha256': this._addRow(Rows.EntryRow, {
                        title: _('Checksum'),
                        subtitle: _('The sha256sum of the file'),
                    }),
                    'filename': this._addRow(Rows.EntryRow, { title: _('File name') }),
                    'size': this._addRow(Rows.SpinRow, { title: _('Size') }),
                },
            ],
            'buildsystem': this._addRow(Rows.ComboRow, {
                title: _('Build System'),
                options: [
                    {
                        title: _('Meson'),
                        value: 'meson',
                    },
                    {
                        title: _('Autotools'),
                        value: 'autotools',
                    },
                    {
                        title: _('CMake'),
                        value: 'cmake',
                    },
                    {
                        title: _('CMake (Ninja)'),
                        value: 'cmake-ninja',
                    },
                    {
                        title: _('QMake'),
                        value: 'qmake',
                    },
                ],
            }),
            'config-opts': this._addRow(Rows.StrvRow, { title: _('Build Options') }),
            'secret-opts': this._addRow(Rows.StrvRow, { title: _('Secrets') }),
            'cleanup': this._addRow(Rows.StrvRow, { title: _('Cleanup') }),
            'post-install': this._addRow(Rows.StrvRow, { title: _('Post-Install Commands') }),
        };

        this._data['name'].bind_property('text', this, 'title', 0);
        this._data['sources'][0]['url'].bind_property('text', this._urlLabel, 'label', 0);

        this._data['sources'][0]['type'].connect('notify::selected', () => this._updateType());
        this._updateType();
    }

    _updateType() {
        const mainSource = this._data['sources'][0];
        const typeRow = mainSource['type'];
        const value = typeRow.model.get_item(typeRow.selected)?.value;

        if (!value)
            return;

        switch(value) {
        case 'archive':
            this._icon.iconName = 'package-x-generic-symbolic';
            break;
        case 'extra-data':
            this._icon.iconName = 'folder-download-symbolic';
            break;
        case 'git':
            this._icon.iconName = 'git-branch-symbolic';
            break;
        }

        mainSource['url'].visible = value === 'git';
        mainSource['commit'].visible = value === 'git';
        mainSource['tag'].visible = value === 'git';

        mainSource['url'].visible |= value === 'archive';
        mainSource['sha256'].visible = value === 'archive';

        mainSource['url'].visible |= value === 'extra-data';
        mainSource['sha256'].visible |= value === 'extra-data';
        mainSource['filename'].visible = value === 'extra-data';
        mainSource['size'].visible = value === 'extra-data';

        this._update();
    }

    _addRow(constructor, option) {
        const row = new constructor(option);

        row.connect('update', () => this._update());

        this.add_row(row);
        return row;
    }

    _validate() {
        let valid = true;

        const validateEntryRow = row => {
            let result = true;
            if (!row.text) {
                row.add_css_class('error');
                result = false;
            } else {
                row.remove_css_class('error');
            }
            return result;
        };

        // Module name
        valid &= validateEntryRow(this._data['name']);

        const mainSource = this._data['sources'][0];
        const typeRow = mainSource['type'];
        const type = typeRow.model.get_item(typeRow.selected)?.value;

        valid &= validateEntryRow(mainSource['url']);

        switch(type) {
        case 'archive':
            valid &= validateEntryRow(mainSource['sha256']);
            break;

        case 'extra-data':
            valid &= validateEntryRow(mainSource['sha256']);
            break;

        case 'git':
            if (!mainSource['commit'].text && !mainSource['tag'].text) {
                mainSource['commit'].add_css_class('error');
                mainSource['tag'].add_css_class('error');
                valid = false;
            } else {
                mainSource['commit'].remove_css_class('error');
                mainSource['tag'].remove_css_class('error');
            }
            break;
        }

        return valid;
    }

    _update() {
        this.emit('update');
    }

    grabFocus() {
        this._data['name'].grab_focus();
    }

    toJSON() {
        if (!this._validate())
            return undefined;

        return {
            'name': this._data['name'],
            'buildsystem': this._data['buildsystem'],
            'config-opts': this._data['config-opts'],
            'secret-opts': this._data['secret-opts'],
            'cleanup': this._data['cleanup'],
            'post-install': this._data['post-install'],
            'sources': this._data['sources'],
        };
    }
});

var ModulesSection = GObject.registerClass({
    GTypeName: 'FmwModulesSection',
}, class ModulesSection extends Section.BaseSection {
    _init() {
        super._init({
            child: new Gtk.ListBox({
                cssClasses: ['content'],
                selectionMode: Gtk.SelectionMode.NONE,
            }),
        });

        this._sources = [];

        this._addNewModuleRow();

        this.child.connect('row-activated', (listbox, row) => {
            if (row !== this._newModuleRow)
                return;

            const newRow = this._addRow({});
            this._addNewModuleRow();
            newRow.expanded = true;
            newRow.grabFocus();
        });
    }

    _addNewModuleRow() {
        if (!this._newModuleRow) {
            this._newModuleRow = new Adw.PreferencesRow({
                child: new Gtk.Label({
                    label: _('Add new module…'),
                    marginTop: 12,
                    marginBottom: 12,
                }),
            });
        } else {
            this.child.remove(this._newModuleRow);
        }

        this.child.append(this._newModuleRow);
    }

    _addRow(module) {
        const newRow = new FmwModuleRow();
        this.child.append(newRow);
        this._sources.push(newRow);

        newRow.connect('update', () => this._update());

        return newRow;
    }

    toJSON() {
        return this._sources;
    }

    addModule(module) {
        const newRow = this._addRow(module);
        this._addNewModuleRow();
        this._update();
    }
});
