/* utils.js
 *
 * Copyright 2021 Georges Basile Stavracas Neto
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

const { Adw, GLib, GObject, Gtk } = imports.gi;

var Row = GObject.registerClass({
    GTypeName: 'FmwRow',
    Signals: { 'update': {} },
    Requires: [Gtk.Widget],
}, class Row extends GObject.Interface {
});

var CheckRow = GObject.registerClass({
    GTypeName: 'FmwCheckRow',
    Implements: [Row],
}, class CheckRow extends Adw.ActionRow {
    _init(params = {}) {
        super._init({
            title: params.title ?? null,
            subtitle: params.subtitle ?? null,
        });

        this._check = new Gtk.CheckButton({ valign: Gtk.Align.CENTER });
        this.add_prefix(this._check);
        this.set_activatable_widget(this._check);

        this._params = params;

        this._check.connect('notify::active', () => this._update());
    }

    _update() {
        this.emit('update');
    }

    grabFocus() {
        this._check.grab_focus();
    }

    toJSON() {
        if (!this.visible)
            return undefined;

        return this._check.active ? this._params.value : undefined;
    }
});

var ComboRow = GObject.registerClass({
    GTypeName: 'FmwComboRow',
    Implements: [Row],
}, class ComboRow extends Adw.ComboRow {
    _init(params = {}) {
        const model = new Gtk.StringList();

        super._init({
            title: params.title ?? null,
            subtitle: params.subtitle ?? null,
            model,
        });

        for (const i in params.options) {
            model.append(params.options[i].title);
            model.get_item(i).value = params.options[i].value;
        }

        this.connect('notify::selected', () => this._update());
    }

    _update() {
        this.emit('update');
    }

    grabFocus() {
        this.grab_focus();
    }

    toJSON() {
        if (!this.visible)
            return undefined;

        return this.model.get_item(this.selected)?.value;
    }
});

var EntryRow = GObject.registerClass({
    GTypeName: 'FmwEntryRow',
    Implements: [Row],
}, class EntryRow extends Adw.EntryRow {
    _init(params = {}) {
        super._init({
            title: params.title ?? null,
            // subtitle: params.subtitle ?? null,
        });

        /*
        this._entry = new Gtk.Entry({
            valign: Gtk.Align.CENTER,
            widthChars: params.widthChars ?? 20,
            inputPurpose: params.inputPurpose ?? Gtk.InputPurpose.FREE_FORM,
        });
        */

        this._validateFunc = params.validateFunc ?? (() => true);

        this.connect('notify::text', () => this._update());
    }

    _validate() {
        let valid = true;

        // Module name
        if (!this._validateFunc(this)) {
            this.add_css_class('error');
            valid = false;
        } else {
            this.remove_css_class('error');
        }

        return valid;
    }

    _update() {
        this.emit('update');
    }

    grabFocus() {
        this.grab_focus();
    }

    toJSON() {
        const { text } = this;

        if (!this._validate() || !this.visible || !text)
            return undefined;

        return text;
    }
});

var SpinRow = GObject.registerClass({
    GTypeName: 'FmwSpinRow',
    Implements: [Row],
}, class SpinRow extends Adw.ActionRow {
    _init(params = {}) {
        super._init({
            title: params.title ?? null,
            subtitle: params.subtitle ?? null,
        });

        this._spin = new Gtk.SpinButton({
            valign: Gtk.Align.CENTER,
            numeric: true,
            digits: 0,
            widthChars: 10,
        });
        this._spin.adjustment.set({
            lower: 0,
            upper: GLib.MAXINT32,
            pageSize: 1,
            pageIncrement: 100,
            stepIncrement: 1,
        });

        this.add_suffix(this._spin);
        this.set_activatable_widget(this._spin);

        this._spin.connect('value-changed', () => this._update());
    }

    _update() {
        this.emit('update');
    }

    grabFocus() {
        this._spin.grab_focus();
    }

    toJSON() {
        if (!this.visible)
            return undefined;

        return this._spin.value;
    }
});

var StrvRow = GObject.registerClass({
    GTypeName: 'FmwStrvRow',
    Implements: [Row],
}, class StrvRow extends Adw.ActionRow {
    _init(params = {}) {
        super._init({
            title: params.title ?? null,
            subtitle: params.subtitle ?? null,
        });

        this._validateFunc = params.validateFunc ?? (() => true);

        this._box = new Gtk.Box({
            valign: Gtk.Align.CENTER,
            orientation: Gtk.Orientation.VERTICAL,
            spacing: 3,
            marginTop: 6,
            marginBottom: 6,
        });
        this.add_suffix(this._box);
        this.set_activatable_widget(this._box);

        this._values = [];

        this._appendStubRow();
    }

    _removeRow(row) {
        const index = this._values.indexOf(row);

        if (index !== -1) {
            this._values.splice(index, 1);
            this._box.remove(row);
        }
    }

    _removeEmptyRows() {
        let child = this._box.get_first_child()
        while (child) {
            const next = child.get_next_sibling();

            if (child !== this._stubRow && !child._entry.text.trim())
                this._removeRow(child);

            child = next;
        }
    }

    _appendStubRow() {
        if (this._stubRow) {
            if (!this._stubRow._entry.text.trim())
                return;

            this._values.push(this._stubRow);
            this._stubRow._button.sensitive = true;
            delete this._stubRow;
        }

        const box = new Gtk.Box({ cssClasses: ['linked'] });
        this._stubRow = box;

        const controller = new Gtk.EventControllerFocus();
        controller.connect('leave', () => {
            GLib.idle_add(GLib.PRIORITY_DEFAULT, () => {
                this._removeEmptyRows();
                return GLib.SOURCE_REMOVE;
            });
        });
        box.add_controller(controller);

        const entry = new Gtk.Entry();
        box.append(entry);
        box._entry = entry;
        entry.connect('activate', () => {
            const next = box.get_next_sibling();
            if (next)
                next._entry.grab_focus();
        });
        entry.connect('notify::text', () => {
            if (box === this._stubRow)
                this._appendStubRow();
            this._update();
        });

        const button = new Gtk.Button({
            sensitive: false,
            iconName: 'edit-clear-symbolic',
        });
        box.append(button);
        box._button = button;
        button.connect('clicked', () => {
            this._removeRow(box);
            this._update();
        })

        box.toJSON = () => entry.text.trim() ? entry.text.trim() : undefined;

        this._box.append(this._stubRow);
    }

    _update() {
        this.emit('update');
    }

    toJSON() {
        if (!this.visible)
            return undefined;

        const values = this._values.filter(row => row.toJSON() || false);
        return values.length > 0 ? values : undefined;
    }

    addValue(value) {
        this._stubRow.text = value;
    }

    grabFocus() {
        this._box.grab_focus();
    }
});

var SwitchRow = GObject.registerClass({
    GTypeName: 'FmwSwitchRow',
    Implements: [Row],
}, class SwitchRow extends Adw.ActionRow {
    _init(params = {}) {
        super._init({
            title: params.title ?? null,
            subtitle: params.subtitle ?? null,
        });

        this._switch = new Gtk.Switch({ valign: Gtk.Align.CENTER });
        this.add_suffix(this._switch);
        this.set_activatable_widget(this._switch);

        this._params = params;

        this._switch.connect('notify::active', () => this._update());
    }

    _update() {
        this.emit('update');
    }

    grabFocus() {
        this._switch.grab_focus();
    }

    toJSON() {
        if (!this.visible)
            return undefined;

        return this._switch.active ? this._params.value : undefined;
    }
});
