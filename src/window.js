/* window.js
 *
 * Copyright 2021 Georges Basile Stavracas Neto
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

const { Adw, GLib, GObject, Gtk, GtkSource } = imports.gi;

const Cleanup = imports.cleanup;
const FileSystem = imports.filesystem;
const Utils = imports.utils;
const Modules = imports.modules;
const Rows = imports.rows;
const Section = imports.section;

const EntryType = {
    ENTRY: 0,
    CHECK: 1,
    SWITCH: 2,
    EXCLUSIVE_CHOICES: 3,
    COMBO: 4,
};

function validateAppId(appId) {
    return appId && appId.match(/([a-zA-Z0-9]+)(\.[a-zA-Z0-9\-]+){2,}/);
}

const SECTIONS = [
    {
        name: null,
        description: null,
        entries: [
            {
                title: _('Application ID'),
                subtitle: _('The unique reverse-domain name of the application.'),
                type: EntryType.ENTRY,
                value: 'app-id',
                validateFunc: row => validateAppId(row.text.trim()),
            },
            {
                title: _('Command'),
                subtitle: _('The command to start the application.'),
                type: EntryType.ENTRY,
                value: 'command',
                validateFunc: row => row.text?.length > 0,
            },
            {
                title: _('Runtime'),
                subtitle: _('A pre-packaged set of dependencies.'),
                type: EntryType.COMBO,
                value: 'runtime',
            },
            {
                title: _('SDK'),
                subtitle: _('Toolkit used to build the application.'),
                type: EntryType.COMBO,
                value: 'sdk',
            },
        ],
    },
    {
        name: _('Permissions'),
        description: _('System services and components your application can access. Only add strictly necessary permissions, and user portals where possible.'),
        entries: [
            {
                title: _('Network'),
                subtitle: _('Allow the application to access the internet.'),
                type: EntryType.SWITCH,
                section: 'finish-args',
                value: '--share=network',
            },
            {
                title: _('Printing'),
                subtitle: _('Allow the application to access the CUPS printing service.'),
                type: EntryType.SWITCH,
                section: 'finish-args',
                value: '--socket=cups',
            },
            {
                title: _('Audio'),
                subtitle: _('Allow the application to access the PulseAudio audio server.'),
                type: EntryType.SWITCH,
                section: 'finish-args',
                value: '--socket=pulseaudio',
            },
            {
                title: _('SSH'),
                subtitle: _('Allow the application to access the Secure Shell agent.'),
                type: EntryType.SWITCH,
                section: 'finish-args',
                value: '--socket=ssh-auth',
            },
        ],
    },
    {
        name: _('Display Server'),
        description: _('The methods your application uses to display windows on various systems. X11 can be set up as a fallback for Wayland, when Wayland is unavailable.'),
        entries: [
            {
                title: _('Wayland'),
                subtitle: _('Display application windows using Wayland.'),
                type: EntryType.SWITCH,
                section: 'finish-args',
                value: '--socket=wayland',
            },
            // this switch row should be insensitive when Wayland is not the active protocol
            {
                title: _('Fallback to X11'),
                subtitle: _('When Wayland is unavailable, fall back to X11 for displaying windows.'),
                type: EntryType.SWITCH,
                section: 'finish-args',
                value: '--socket=fallback-x11',
            },
            {
                title: _('X11'),
                subtitle: _('Display application windows using X11 on legacy systems.'),
                type: EntryType.SWITCH,
                section: 'finish-args',
                value: '--socket=x11',
            },
            // this should likely automatically be enabled when X11 is enabled
            {
                title: _('IPC with X11'),
                subtitle: _('Share inter-process communication namespace with host.'),
                type: EntryType.SWITCH,
                section: 'finish-args',
                value: '--share=ipc',
            },
        ],
    },
    {
        name: _('Devices'),
        description: _('What devices your application has access to while sandboxed.'),
        entries: [
            {
                title: _('Bluetooth'),
                subtitle: null,
                type: EntryType.CHECK,
                section: 'finish-args',
                value: '--allow=bluetooth',
            },
            {
                title: _('Graphics Processor'),
                subtitle: null,
                type: EntryType.CHECK,
                section: 'finish-args',
                value: '--device=dri',
            },
            {
                title: _('Smart Cards'),
                subtitle: null,
                type: EntryType.CHECK,
                section: 'finish-args',
                value: '--socket=pcsc',
            },
            {
                title: _('Other Peripherals'),
                subtitle: _('Include webcams, controllers, and third-party devices'),
                type: EntryType.CHECK,
                section: 'finish-args',
                value: '--device=all',
            },
        ],
    },
    {
        name: GLib.markup_escape_text(_('Files & Folders'), -1),
        description: _('The level of access to the file system your application will have access to.'),
        widget: FileSystem.FileSystemSection,
        section: 'finish-args',
        entries: [],
    },
    {
        name: _('D-Bus'),
        description: _('Allow your application to have access to the message bus to communicate with other applications.'),
        entries: [
            {
                title: _('Session bus'),
                subtitle: _('Allow unrestricted access to the D-Bus session bus'),
                type: EntryType.SWITCH,
                section: 'finish-args',
                value: '--socket=session-bus',
            },
            {
                title: _('System bus'),
                subtitle: _('Allow unrestricted access to the D-Bus system bus'),
                type: EntryType.SWITCH,
                section: 'finish-args',
                value: '--socket=system-bus',
            },
        ],
    },
    {
        name: _('Modules'),
        // TODO: come up with a meaningful description for this section
        description: null,
        widget: Modules.ModulesSection,
        value: 'modules',
        entries: [],
    },
    {
        name: _('Post-Build Cleanup'),
        description: _('Files and folders to remove after the build is finished. Removing unnecessary files can reduce the size of your Flatpak package.'),
        widget: Cleanup.CleanupSection,
        value: 'cleanup',
        entries: [],
    },
];

var FmeWindow = GObject.registerClass({
    GTypeName: 'FmeWindow',
    Template: 'resource:///org/flatpak/ManifestEditor/window.ui',
    InternalChildren: ['preferencesPage', 'buffer']
}, class FmeWindow extends Adw.ApplicationWindow {
    _init(application) {
        super._init({ application });

        const languageManager = GtkSource.LanguageManager.get_default();
        this._buffer.language = languageManager.get_language('json');

        this._sections = {};

        for (const section of SECTIONS)
            this._addSection(section);

        this._update();
    }

    _addSection(section) {
        const group = new Adw.PreferencesGroup({
            title: section.name,
            description: section.description,
        });

        let showMore = false;

        for (const entry of section.entries) {
            this._addEntry(group, entry);
            showMore |= entry.hidden;
        }

        if (showMore) {
            const row = new Adw.PreferencesRow({
                child: new Gtk.Label({
                    label: _('Show More…'),
                    marginTop: 12,
                    marginBottom: 12,
                }),
            });
            group.add(row);

            const listbox = row.get_parent();
            const id = listbox.connect('row-activated', (listbox, activatedRow) => {
                if (activatedRow !== row)
                    return;

                for (let child = listbox.get_first_child(); child; child = child.get_next_sibling())
                    child.show();

                group.remove(row);
                listbox.disconnect(id);
            });
        }

        if (section.widget) {
            const widget = new section.widget();
            widget.connect('update', () => this._update());
            group.add(widget);

            if (section.section)
                this._sections[section.section].push(widget);
            else if (section.value)
                this._sections[section.value] = widget;
        }

        this._preferencesPage.add(group);
    }

    _addEntry(group, entry) {
        let row = null;

        switch (entry.type) {
        case EntryType.ENTRY:
            row = this._createRow(Rows.EntryRow, entry);
            break;

        case EntryType.CHECK:
            row = this._createRow(Rows.CheckRow, entry);
            break;

        case EntryType.SWITCH:
            row = this._createRow(Rows.SwitchRow, entry);
            break;

        case EntryType.COMBO:
            row = this._createRow(Rows.ComboRow, entry);

            // Let combo rows that initialize their model asynchronously
            // do that
            if (entry.initializeModel)
                entry.initializeModel(row);
            break;
        }

        row.visible = !entry.hidden;

        this._addRowToEntries(row, entry);

        group.add(row);
    }

    _createRow(constructor, entry) {
        const row = new constructor(entry);
        row.connect('update', () => this._update());
        return row;
    }

    _addRowToEntries(row, entry) {
        if (entry.section && !this._sections[entry.section])
            this._sections[entry.section] = [];

        // Add entry to section
        if (entry.section)
            this._sections[entry.section].push(row);
        else
            this._sections[entry.value] = row;
    }

    _update() {
        const clean = Utils.cleanObject(this.toJSON());
        const str = JSON.stringify(clean, (k, v) => v ?? undefined, '  ');
        this._buffer.set_text(str, -1);
    }

    _onOpenButtonClicked() {
        const dialog = new Gtk.FileChooserNative({
            action: Gtk.FileChooserAction.OPEN,
            transientFor: this,
            modal: true,
        });

        const filter = new Gtk.FileFilter();
        filter.set_name(_('JSON'));
        filter.add_mime_type('application/json');
        dialog.add_filter(filter);

        dialog.connect("response", (self, response) => {
            if (response === Gtk.ResponseType.ACCEPT) {
                log(dialog.get_file());
                this._update();
            }

            dialog.destroy();
        });

        dialog.show();
    }

    toJSON() {
        return {
            'app-id': this._sections['app-id'],
            'command': this._sections['command'],
            'finish-args': this._sections['finish-args'],
            'cleanup': this._sections['cleanup'],
            'modules': this._sections['modules'],
        };
    }
});
