/* main.js
 *
 * Copyright 2021 Georges Basile Stavracas Neto
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

imports.gi.versions.GtkSource = '5';

pkg.initGettext();
pkg.initFormat();
pkg.require({
  'Gio': '2.0',
  'Gtk': '4.0',
  'GtkSource': '5',
});

const { Adw, Gio, Gtk , GtkSource } = imports.gi;
const Gettext = imports.gettext;

const { FmeWindow } = imports.window;

function main(argv) {
    globalThis._ = Gettext.gettext;

    const application = new Adw.Application({
        application_id: 'org.flatpak.ManifestEditor',
        flags: Gio.ApplicationFlags.FLAGS_NONE,
    });

    application.connect('startup', app => {
        GtkSource.init();
    });

    application.connect('activate', app => {
        let activeWindow = app.activeWindow;

        if (!activeWindow)
            activeWindow = new FmeWindow(app);

        activeWindow.present();
    });

    return application.run(argv);
}
