/* utils.js
 *
 * Copyright 2021 Georges Basile Stavracas Neto
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

function unwrapArray(array) {
    if (!(array instanceof Array))
        return array;

    const newArray = [];

    const expand = a => {
        a.forEach(item => {
            if (!item)
                return;

            if (item instanceof Array) {
                expand(item);
            } else {
                const itemJson = item.toJSON ? item.toJSON() : item;
                if (itemJson !== null && itemJson !== undefined)
                    newArray.push(itemJson);
            }
        });
    };
    expand(array);

    return newArray;
}

function cleanObject(object) {
    const newObject = {};
    for (const key in object) {
        if (!key || object[key] === null || object[key] === undefined)
            continue;

        const json = object[key].toJSON ? object[key].toJSON() : object[key];
        if (json === null || json === undefined)
            continue;

        if (json instanceof Array) {
            const items = [];
            for (const item of json) {
                const itemJson = item.toJSON ? item.toJSON() : item;
                if (itemJson !== null && itemJson !== undefined)
                    items.push(itemJson);
            }
            newObject[key] = unwrapArray(items).sort();
        } else if (json instanceof Object) {
            newObject[key] = cleanObject(json);
        } else {
            newObject[key] = json;
        }
    }
    return newObject;
}
