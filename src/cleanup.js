/* cleanup.js
 *
 * Copyright 2021 Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const { Adw, GLib, GObject, Gtk } = imports.gi;

const Rows = imports.rows;
const Section = imports.section;

const CleanupRow = GObject.registerClass({
    GTypeName: 'FmwCleanupRow',
    Implements: [Rows.Row],
    Signals: { 'delete': {} },
}, class CleanupRow extends Gtk.Box {
    _init() {
        super._init({ cssClasses: ['linked'] });

        this._entry = new Gtk.Entry({
            hexpand: true,
            valign: Gtk.Align.CENTER,
            widthChars: 30,
        });
        this.append(this._entry);

        this._button = new Gtk.Button({
            sensitive: false,
            iconName: 'edit-delete-symbolic',
        });
        this._button.connect('clicked', () => this.emit('delete'));
        this.append(this._button);

        this._entry.connect('notify::text', () => this._update());

        this._focusController = new Gtk.EventControllerFocus();
        this._focusController.connect('leave', () => {
            GLib.idle_add(GLib.PRIORITY_DEFAULT, () => {
                this._update();
                return GLib.SOURCE_REMOVE;
            });
        });
        this.add_controller(this._focusController);
    }

    _update() {
        if (this.text?.length > 0)
            this._button.sensitive = true;

        this.emit('update');
    }

    grabFocus() {
        this._entry.grab_focus();
    }

    toJSON() {
        const { text } = this;

        if (!this.visible || !text)
            return undefined;

        return text;
    }

    get entry() {
        return this._entry;
    }

    get text() {
        return this._entry.text.trim();
    }

    set text(v) {
        this._entry.text = v.trim();
    }

    get containsFocus() {
        return this._focusController.containsFocus;
    }
});

var CleanupSection = GObject.registerClass({
    GTypeName: 'FmwCleanupSection',
}, class CleanupSection extends Section.BaseSection {
    _init() {
        super._init({
            child: new Gtk.Box({
                cssClasses: ['content'],
                orientation: Gtk.Orientation.VERTICAL,
                spacing: 6,
            }),
        });

        this._rows = [];

        this._appendRow();
    }

    _removeRow(row) {
        const index = this._rows.indexOf(row);

        if (index !== -1) {
            this._rows.splice(index, 1);
            this.child.remove(row);
        }
    }

    _removeEmptyRows() {
        let child = this.child.get_first_child()
        while (child) {
            const next = child.get_next_sibling();

            if (child !== this._emptyRow && !child.text && !child.containsFocus)
                this._removeRow(child);

            child = next;
        }
    }

    _appendRow(params) {
        if (this._emptyRow) {
            if (!this._emptyRow.text)
                return;

            this._rows.push(this._emptyRow);
            delete this._emptyRow;
        }

        const row = new CleanupRow(params);
        this._emptyRow = row;

        row.entry.connect('activate', () => {
            const next = row.get_next_sibling();
            if (next)
                next.entry.grab_focus();
        });
        row.connect('update', () => {
            if (row === this._emptyRow)
                this._appendRow();
            this._update();
        });
        row.connect('delete', () => this._removeRow(row));

        this.child.append(row);
    }

    _update() {
        this._removeEmptyRows();
        super._update();
    }

    toJSON() {
        return this._rows;
    }

    addRow(params) {
        this._appendRow(params);
        this._update();
    }
});
