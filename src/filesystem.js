/* filesystem.js
 *
 * Copyright 2021 Georges Basile Stavracas Neto
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

const { Adw, GLib, GObject, Gtk } = imports.gi;

const Rows = imports.rows;
const Section = imports.section;

const FOLDERS = {
    'toplevel': {
        title: _('Filesystem Access'),
        iconName: 'user-desktop-symbolic',
        options: [
            {
                title: _('Sandboxed'),
                subtitle: _('Access to files and folders can only be made using portals.'),
                iconName: 'portal-symbolic',
                value: undefined,
            },
            {
                title: _('Specific Folders'),
                subtitle: _('Only the selected folders are accessible to the application.'),
                iconName: 'system-lock-screen-symbolic',
                value: undefined,
                showStandardFolders: true,
                showCustomFolders: true,
            },
            {
                title: _('Full Home Access'),
                subtitle: _('Application can access most files under the home directory.'),
                iconName: 'user-home-symbolic',
                value: 'home',
                showCustomFolders: true,
            },
            {
                title: _('Full System Access'),
                subtitle: _('Application can access most files on your computer.'),
                iconName: 'computer-symbolic',
                value: 'host',
                showCustomFolders: true,
            },
        ],
    },
    'xdg-documents': {
        title: _('Documents'),
        iconName: 'folder-documents-symbolic',
    },
    'xdg-download': {
        title: _('Download'),
        iconName: 'folder-download-symbolic',
    },
    'xdg-pictures': {
        title: _('Images'),
        iconName: 'folder-pictures-symbolic',
    },
    'xdg-music': {
        title: _('Music'),
        iconName: 'folder-music-symbolic',
    },
    'xdg-templates': {
        title: _('Templates'),
        iconName: 'folder-templates-symbolic',
    },
    'xdg-videos': {
        title: _('Videos'),
        iconName: 'folder-videos-symbolic',
    },
    'xdg-cache': {
        title: _('User Cache'),
        subtitle: _('The user cache directory, usually ~/.cache'),
        iconName: 'preferences-system-symbolic',
    },
    'xdg-config': {
        title: _('User Config'),
        subtitle: _('The user config directory, usually ~/.config'),
        iconName: 'preferences-system-symbolic',
    },
    'xdg-data': {
        title: _('User Data'),
        subtitle: _('The user data directory, usually ~/.local/share'),
        iconName: 'preferences-system-symbolic',
    },
};

const ToplevelFolderRow = GObject.registerClass({
    GTypeName: 'FmwToplevelFolderRow',
}, class ToplevelFolderRow extends Rows.ComboRow {
    _init(folder) {
        const info = FOLDERS[folder];

        super._init(info);

        this._icon = new Gtk.Image();
        this.add_prefix(this._icon);

        this._folder = folder;

        this._update();
    }

    _update() {
        const { selected, model } = this;

        this._icon.iconName = FOLDERS['toplevel'].options[selected].iconName;

        const showRows = model.get_item(selected)?.value !== undefined;
        this.subtitle = FOLDERS['toplevel'].options[selected].subtitle;

        super._update();
    }

    toJSON() {
        const { value } = FOLDERS['toplevel'].options[this.selected];

        if (!this.visible || !value)
            return undefined;

        return '--filesystem=' + value;
    }
});

const StandardFolderRow = GObject.registerClass({
    GTypeName: 'FmwStandardFolderRow',
}, class StandardFolderRow extends Rows.SwitchRow {
    _init(folder) {
        const info = FOLDERS[folder];

        super._init(info);

        const icon = new Gtk.Image({ iconName: info.iconName });
        this.add_prefix(icon);

        this._folder = folder;
    }

    toJSON() {
        if (!this.visible || !this._switch.active)
            return undefined;
        return '--filesystem=' + this._folder;
    }
});


const CustomFolderRow = GObject.registerClass({
    GTypeName: 'FmwCustomFolderRow',
}, class CustomFolderRow extends Rows.EntryRow {
    _init() {
        super._init(info);

        const icon = new Gtk.Image({ iconName: info.iconName });
        this.add_prefix(icon);
    }

    toJSON() {
        if (!this.visible || !this.text)
            return undefined;
        return '--filesystem=' + this.text;
    }
});

var FileSystemSection = GObject.registerClass({
    GTypeName: 'FmwFileSystemSection',
}, class FileSystemSection extends Section.BaseSection {
    _init() {
        super._init({
            child: new Gtk.ListBox({
                cssClasses: ['content'],
                selectionMode: Gtk.SelectionMode.NONE,
            }),
        });

        this._folders = [];
        for (const folder in FOLDERS)
            this._addFolder(folder);

        this._updateVisibleRows();
    }

    _updateVisibleRows() {
        const accessRow = this._folders[0];
        const showStandardFolders =
            FOLDERS['toplevel'].options[accessRow.selected].showStandardFolders;

        this._folders.forEach((row, index) => {
            if (index === 0)
                return;

            row.visible = showStandardFolders;
        });
    }

    _addFolder(folder) {
        const isToplevel = folder === 'toplevel';
        const row = isToplevel
            ? new ToplevelFolderRow(folder)
            : new StandardFolderRow(folder);

        row.connect('update', () => {
            if (isToplevel)
                this._updateVisibleRows();
            this._update();
        });

        this.child.append(row);
        this._folders.push(row);
    }

    toJSON() {
        return this._folders;
    }

    addFolder(folder) {
        const newRow = this._addRow(folder);
        this._addNewModuleRow();
        this._update();
    }
});
